#!/usr/bin/python

import pymysql
from pymysql import connections
import datetime as datatime
import random
import string

host = 'localhost'
username = 'root'
password = 'Asd%123000'


create_table_sql = """\
    CREATE TABLE person(
        id INT AUTO_INCREMENT PRIMARY KEY,
        name varchar(20) NOT NULL ,
        birthday DATE ,
        gender char(10) NOT NULL
    )
"""


insert_table_sql = """\
    INSERT INTO person(name,birthday,gender)
    VALUES('{username}','{birthday}','{gender}');
    """
query_table_sql = """\
    SELECT *from person;
    """

delete_table_sql = """\
    DELETE FROM person
    """

drop_table_sql = """\
    DROP TABLE person
    """


def connect_wxremit_db(db_name):
    return pymysql.connect(host=host,
                           user=username,
                           password=password,
                           charset='utf8mb4',
                           database=db_name)


def randomStr(num):
    salt = ''.join(random.sample(string.ascii_letters+string.digits, num))
    return salt


def run_connection(connection):
    try:
        with connection.cursor() as cursor:

            # cursor.execute(create_table_sql)
            # connection.commit()
            i = 0
            while i < 800:
                str = randomStr(5)
                cursor.execute(
                    insert_table_sql.format(username=str,
                                            birthday='2202-12-3', gender='m')
                )
                i += 1
            connection.commit()
            print("数据库插入成功.....")
            # cursor.execute(query_table_sql)
            # res = cursor.fetchall()
            # for row in res:
            #     print(row[0], row[1], row[2], row[3], sep='\t')
            cursor.execute(delete_table_sql)
            connection.commit
            print("数据库删数据成功.....")

    finally:
        pass


i = 0
while i < 1000:
    connection = connect_wxremit_db('testdb1')
    i += 1
    run_connection(connection)


print("----------------------------------\n----------------------所有连接创建完成---------！")
while 1:
    pass
